package br.com.infrastructure.domain

import br.com.infrastructure.data.rest.CreateUserBody
import io.reactivex.Observable


interface CreateUser {

    fun create(createUserBody: CreateUserBody): Observable<StatusMessage>

}