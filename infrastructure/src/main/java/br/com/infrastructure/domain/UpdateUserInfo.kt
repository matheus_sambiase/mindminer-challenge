package br.com.infrastructure.domain

import br.com.infrastructure.data.rest.UpdateUserInfoBody
import io.reactivex.Observable


interface UpdateUserInfo {

    fun update(updateUserInfoBody: UpdateUserInfoBody): Observable<StatusMessage>
}