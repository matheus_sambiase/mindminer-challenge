package br.com.infrastructure.domain


sealed class NetworkError : Throwable() {

    class InternetUnavailable : NetworkError()
    class Timeout : NetworkError()
    class NetworkHiccup : NetworkError()

}