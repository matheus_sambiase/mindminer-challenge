package br.com.infrastructure.domain


sealed class RequestError : Throwable() {

    class InvalidToken : RequestError()
    class UserNotFound : RequestError()
    class UserTaken : RequestError()
    class UnknowResponse : RequestError()

}