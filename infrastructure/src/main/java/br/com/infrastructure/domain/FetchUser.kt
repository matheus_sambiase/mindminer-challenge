package br.com.infrastructure.domain

import br.com.infrastructure.data.rest.FetchUserBody
import io.reactivex.Observable


interface FetchUser {

    fun fetch(fetchUserBody: FetchUserBody): Observable<User>

}