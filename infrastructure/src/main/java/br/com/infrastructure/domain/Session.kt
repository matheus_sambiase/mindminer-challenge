package br.com.infrastructure.domain


data class Session(var token: String)