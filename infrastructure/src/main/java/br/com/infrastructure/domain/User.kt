package br.com.infrastructure.domain


data class User(var username: String,
                var firstName: String,
                var lastName: String)