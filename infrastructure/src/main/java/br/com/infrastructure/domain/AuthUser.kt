package br.com.infrastructure.domain

import br.com.infrastructure.data.rest.AuthBody
import io.reactivex.Observable


interface AuthUser {

    fun auth(authBody: AuthBody): Observable<Session>

}