package br.com.infrastructure.data.payload

import com.google.gson.annotations.SerializedName


data class AuthUserContentPayload(
        @SerializedName("token") var token: String
)