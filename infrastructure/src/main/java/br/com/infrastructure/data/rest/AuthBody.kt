package br.com.infrastructure.data.rest


data class AuthBody(
        var username: String,
        var password: String
)