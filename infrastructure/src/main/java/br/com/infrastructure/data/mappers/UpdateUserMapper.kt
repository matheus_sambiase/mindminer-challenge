package br.com.infrastructure.data.mappers

import br.com.infrastructure.data.payload.UpdateUserInfoPayload
import br.com.infrastructure.domain.StatusMessage


class UpdateUserMapper {

    fun toStatusMessage(payload: UpdateUserInfoPayload): StatusMessage {

        return StatusMessage(
                message = payload.message
        )
    }
}