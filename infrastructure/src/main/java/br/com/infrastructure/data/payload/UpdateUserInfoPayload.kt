package br.com.infrastructure.data.payload

import com.google.gson.annotations.SerializedName


data class UpdateUserInfoPayload(
        @SerializedName("message") var message: String
)