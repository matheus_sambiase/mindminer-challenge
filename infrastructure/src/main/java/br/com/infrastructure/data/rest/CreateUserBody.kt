package br.com.infrastructure.data.rest


data class CreateUserBody(
        var username: String,
        var password: String
)