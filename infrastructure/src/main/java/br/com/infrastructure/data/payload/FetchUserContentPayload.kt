package br.com.infrastructure.data.payload

import com.google.gson.annotations.SerializedName


data class FetchUserContentPayload(
        @SerializedName("username") var username: String,
        @SerializedName("firstName") var firstName: String,
        @SerializedName("lastName") var lastName: String
)