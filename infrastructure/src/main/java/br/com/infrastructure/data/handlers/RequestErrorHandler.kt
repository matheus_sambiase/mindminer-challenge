package br.com.infrastructure.data.handlers

import br.com.infrastructure.domain.RequestError
import br.com.infrastructure.domain.RequestError.*
import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.ObservableTransformer
import org.json.JSONObject
import retrofit2.HttpException


class RequestErrorHandler<T> : ObservableTransformer<T, T> {

    override fun apply(upstream: Observable<T>): ObservableSource<T> {
        return upstream.onErrorResumeNext(this::handleIfRestError)
    }

    private fun handleIfRestError(throwable: Throwable): Observable<T> {
        if (isRestError(throwable)) return asRequestError(throwable as HttpException)
        return Observable.error(throwable)
    }

    private fun asRequestError(restError: HttpException): Observable<T> {
        val errorBody = JSONObject(restError.response().errorBody()?.string()).getString("message")
        val dataAccessError = mapErrorWith(errorBody)
        return Observable.error(dataAccessError)
    }

    private fun mapErrorWith(message: String): RequestError {
        return when (message) {
            "Username is already in use" -> UserTaken()
            "User not found" -> UserNotFound()
            "Invalid token" -> InvalidToken()
            else -> UnknowResponse()
        }
    }

    private fun isRestError(throwable: Throwable): Boolean {
        return throwable is HttpException
    }
}