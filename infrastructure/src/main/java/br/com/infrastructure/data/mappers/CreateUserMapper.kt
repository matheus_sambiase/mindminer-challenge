package br.com.infrastructure.data.mappers

import br.com.infrastructure.data.payload.CreateUserPayload
import br.com.infrastructure.domain.StatusMessage


class CreateUserMapper {

    fun toStatusMessage(payload: CreateUserPayload): StatusMessage {

        return StatusMessage(
                message = payload.message
        )
    }
}