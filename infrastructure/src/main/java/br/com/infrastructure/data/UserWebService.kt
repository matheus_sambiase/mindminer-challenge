package br.com.infrastructure.data

import br.com.infrastructure.data.handlers.GsonErrorHandler
import br.com.infrastructure.data.handlers.NetworkErrorHandler
import br.com.infrastructure.data.handlers.RequestErrorHandler
import br.com.infrastructure.data.mappers.AuthUserMapper
import br.com.infrastructure.data.mappers.CreateUserMapper
import br.com.infrastructure.data.mappers.FetchUserMapper
import br.com.infrastructure.data.mappers.UpdateUserMapper
import br.com.infrastructure.data.payload.FetchUserPayload
import br.com.infrastructure.data.payload.UpdateUserInfoPayload
import br.com.infrastructure.data.rest.FetchUserBody
import br.com.infrastructure.data.rest.MindminersWebServiceInterface
import br.com.infrastructure.data.rest.UpdateUserInfoBody
import br.com.infrastructure.domain.FetchUser
import br.com.infrastructure.domain.StatusMessage
import br.com.infrastructure.domain.UpdateUserInfo
import br.com.infrastructure.domain.User
import io.reactivex.Observable
import io.reactivex.Scheduler


class UserWebService(private val webservice: MindminersWebServiceInterface,
                     private val ioBoundedScheduler: Scheduler) : FetchUser, UpdateUserInfo {

    private val authMapper = AuthUserMapper()
    private val createUserMapper = CreateUserMapper()
    private val fetchUserMapper = FetchUserMapper()
    private val updateUserMapper = UpdateUserMapper()

    override fun fetch(fetchUserBody: FetchUserBody): Observable<User> {
        return webservice
                .fetchUser(fetchUserBody)
                .subscribeOn(ioBoundedScheduler)
                .compose(RequestErrorHandler<FetchUserPayload>())
                .compose(NetworkErrorHandler<FetchUserPayload>())
                .compose(GsonErrorHandler<FetchUserPayload>())
                .map { content -> fetchUserMapper.toUser(content) }
    }

    override fun update(updateUserInfoBody: UpdateUserInfoBody): Observable<StatusMessage> {
        return webservice
                .updateUserInfo(updateUserInfoBody)
                .subscribeOn(ioBoundedScheduler)
                .compose(RequestErrorHandler<UpdateUserInfoPayload>())
                .compose(NetworkErrorHandler<UpdateUserInfoPayload>())
                .compose(GsonErrorHandler<UpdateUserInfoPayload>())
                .map { content -> updateUserMapper.toStatusMessage(content) }
    }
}