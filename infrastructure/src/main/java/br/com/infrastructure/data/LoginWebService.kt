package br.com.infrastructure.data

import br.com.infrastructure.data.handlers.GsonErrorHandler
import br.com.infrastructure.data.handlers.NetworkErrorHandler
import br.com.infrastructure.data.handlers.RequestErrorHandler
import br.com.infrastructure.data.mappers.AuthUserMapper
import br.com.infrastructure.data.mappers.CreateUserMapper
import br.com.infrastructure.data.mappers.FetchUserMapper
import br.com.infrastructure.data.mappers.UpdateUserMapper
import br.com.infrastructure.data.payload.AuthUserPayload
import br.com.infrastructure.data.payload.CreateUserPayload
import br.com.infrastructure.data.rest.AuthBody
import br.com.infrastructure.data.rest.CreateUserBody
import br.com.infrastructure.data.rest.MindminersWebServiceInterface
import br.com.infrastructure.domain.AuthUser
import br.com.infrastructure.domain.CreateUser
import br.com.infrastructure.domain.Session
import br.com.infrastructure.domain.StatusMessage
import io.reactivex.Observable
import io.reactivex.Scheduler


class LoginWebService(private val webservice: MindminersWebServiceInterface,
                      private val ioBoundedScheduler: Scheduler) : AuthUser, CreateUser{

    private val authMapper = AuthUserMapper()
    private val createUserMapper = CreateUserMapper()
    private val fetchUserMapper = FetchUserMapper()
    private val updateUserMapper = UpdateUserMapper()

    override fun auth(authBody: AuthBody): Observable<Session> {
        return webservice
                .authUser(authBody)
                .subscribeOn(ioBoundedScheduler)
                .compose(RequestErrorHandler<AuthUserPayload>())
                .compose(NetworkErrorHandler<AuthUserPayload>())
                .compose(GsonErrorHandler<AuthUserPayload>())
                .map { content -> authMapper.toSession(content) }
    }

    override fun create(createUserBody: CreateUserBody): Observable<StatusMessage> {
        return webservice
                .createUser(createUserBody)
                .subscribeOn(ioBoundedScheduler)
                .compose(RequestErrorHandler<CreateUserPayload>())
                .compose(NetworkErrorHandler<CreateUserPayload>())
                .compose(GsonErrorHandler<CreateUserPayload>())
                .map { content -> createUserMapper.toStatusMessage(content) }
    }

}