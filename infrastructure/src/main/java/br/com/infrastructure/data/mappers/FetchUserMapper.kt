package br.com.infrastructure.data.mappers

import br.com.infrastructure.data.payload.FetchUserPayload
import br.com.infrastructure.domain.User


class FetchUserMapper {

    fun toUser(payload: FetchUserPayload): User {
        return User(
                username = payload.contentPayload.username,
                firstName = payload.contentPayload.firstName,
                lastName = payload.contentPayload.lastName
        )
    }
}