package br.com.infrastructure.data.payload

import com.google.gson.annotations.SerializedName


data class CreateUserPayload(
        @SerializedName("message") var message: String
)