package br.com.infrastructure.data.rest


data class FetchUserBody(
        var token: String
)