package br.com.infrastructure.data.payload

import com.google.gson.annotations.SerializedName


data class FetchUserPayload(
        @SerializedName("user") var contentPayload: FetchUserContentPayload
)