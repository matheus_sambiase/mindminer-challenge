package br.com.infrastructure.data.mappers

import br.com.infrastructure.data.payload.AuthUserPayload
import br.com.infrastructure.domain.Session


class AuthUserMapper {

    fun toSession(payload: AuthUserPayload): Session {
        return Session(
                token = payload.contentPayload.token
        )
    }
}