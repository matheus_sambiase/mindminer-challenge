package br.com.infrastructure.data.rest

import br.com.infrastructure.data.payload.AuthUserPayload
import br.com.infrastructure.data.payload.CreateUserPayload
import br.com.infrastructure.data.payload.FetchUserPayload
import br.com.infrastructure.data.payload.UpdateUserInfoPayload
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.POST


interface MindminersWebServiceInterface {

    @POST("/user/create")
    fun createUser(@Body createUserBody: CreateUserBody): Observable<CreateUserPayload>

    @POST("auth/authUser")
    fun authUser(@Body authBody: AuthBody): Observable<AuthUserPayload>

    @POST("user/getDetails")
    fun fetchUser(@Body fetchUserBody: FetchUserBody): Observable<FetchUserPayload>

    @POST("user/update")
    fun updateUserInfo(@Body updateUserInfoBody: UpdateUserInfoBody): Observable<UpdateUserInfoPayload>

    companion object {
        val BASE_URL = "http://104.131.189.211:8085/"
    }
}