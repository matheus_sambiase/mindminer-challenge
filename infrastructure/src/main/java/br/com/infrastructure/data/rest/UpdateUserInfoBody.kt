package br.com.infrastructure.data.rest


data class UpdateUserInfoBody(
        var token: String,
        var firstname: String,
        var lastname: String,
        var password: String
)