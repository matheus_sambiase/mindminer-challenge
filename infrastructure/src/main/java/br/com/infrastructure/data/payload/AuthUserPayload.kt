package br.com.infrastructure.data.payload

import com.google.gson.annotations.SerializedName


data class AuthUserPayload(
        @SerializedName("session") var contentPayload: AuthUserContentPayload
)