package br.com.takehomeproject

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.support.v4.app.FragmentActivity


inline fun <reified VM : ViewModel> FragmentActivity.withFactory(
        mode: LazyThreadSafetyMode = LazyThreadSafetyMode.NONE,
        crossinline factory: () -> ViewModelProvider.Factory) = lazy(mode) {

    ViewModelProviders.of(this, factory()).get(VM::class.java)

}