package br.com.takehomeproject.main

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import br.com.infrastructure.domain.StatusMessage
import br.com.infrastructure.domain.User
import br.com.presentation.user.UserView
import br.com.presentation.user.UserViewModel
import br.com.takehomeproject.R
import br.com.takehomeproject.factories.BehaviorsFactory
import br.com.takehomeproject.factories.BehaviorsFactory.uiScheduler
import br.com.takehomeproject.factories.UserFactory
import br.com.takehomeproject.withFactory
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Action
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), UserView {

    var token: String? = null

    val composite by lazy { CompositeDisposable() }
    val coordinator by lazy { BehaviorsFactory(this) }
    val viewModel by withFactory<UserViewModel> { UserFactory() }

    companion object {

        val EXTRA_TOKEN = "EXTRA_TOKEN"

        fun newIntent(activity: AppCompatActivity, token: String): Intent {
            val intent = Intent(activity, MainActivity::class.java)
            intent.putExtra(EXTRA_TOKEN, token)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        token = intent.getStringExtra(EXTRA_TOKEN)

        fetchUser()

        updateUserInfoBtn.setOnClickListener { updateUserInfo() }

    }

    override fun onDestroy() {
        super.onDestroy()
        releaseSubscriptions()
    }

    private fun updateUserInfo() {

        val disposable = viewModel.updateUserInfo(token!!,
                password_input.text.toString(),
                User(username_input.text.toString(),
                        firstname_input.text.toString(),
                        lastname_input.text.toString()))
                .compose(coordinator)
                .subscribeOn(uiScheduler)
                .subscribe({ updatedWithSuccess(it as StatusMessage) }, {})

        composite.add(disposable)
    }

    private fun fetchUser() {

        val disposable = viewModel.fetchUser(token!!)
                .compose(coordinator)
                .subscribeOn(uiScheduler)
                .subscribe({ fetchedWithSuccess(it as User) },
                        {it.printStackTrace()})

        composite.add(disposable)
    }


    fun fetchedWithSuccess(user: User) {
        runOnUiThread {
            firstname_input.setText(user.firstName)
            lastname_input.setText(user.lastName)
            username_input.setText(user.username)
        }
    }

    fun updatedWithSuccess(statusmessage: StatusMessage) {
        showCallToAction(statusmessage.message)
    }

    override fun showLoading(): Action {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showErrorState(): Action {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun reportNetworkingError(): Action {
        return Action { showCallToAction(R.string.no_connection_message_error) }
    }

    override fun hideLoading(): Action {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun hideErrorState(): Action {
        return Action { println("tirando  error state") }
    }

    private fun showCallToAction(actionText: String) {
        Snackbar.make(main_root, actionText, Snackbar.LENGTH_LONG)
                .show()
    }

    private fun showCallToAction(callToActionText: Int) {
        Snackbar.make(main_root, callToActionText, Snackbar.LENGTH_LONG)
                .show()
    }


    private fun releaseSubscriptions() {
        composite.clear()
    }

}