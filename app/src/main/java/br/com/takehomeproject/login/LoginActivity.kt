package br.com.takehomeproject.login

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.ViewGroup
import android.widget.Button
import br.com.infrastructure.domain.Session
import br.com.infrastructure.domain.StatusMessage
import br.com.presentation.login.LoginView
import br.com.presentation.login.LoginViewModel
import br.com.takehomeproject.R
import br.com.takehomeproject.factories.BehaviorsFactory
import br.com.takehomeproject.factories.BehaviorsFactory.uiScheduler
import br.com.takehomeproject.factories.LoginFactory
import br.com.takehomeproject.main.MainActivity
import br.com.takehomeproject.withFactory
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Action
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), LoginView {

    lateinit var loginBtn: Button
    lateinit var createAccBtn: Button
    lateinit var root: ViewGroup
    val composite by lazy { CompositeDisposable() }
    val coordinator by lazy { BehaviorsFactory(this) }
    val viewModel by withFactory<LoginViewModel> { LoginFactory() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        loginBtn = findViewById(R.id.loginBtn)
        createAccBtn = findViewById(R.id.createAccBtn)
        root = findViewById(R.id.login_root)

        loginBtn.setOnClickListener {
            login()
        }

        createAccBtn.setOnClickListener {
            createUser()
        }


    }

    override fun onDestroy() {
        super.onDestroy()
        releaseSubscriptions()
    }

    fun login() {


        val disposable = viewModel.authUser(username_input.text.toString(), password_input.text.toString())
                .compose(coordinator)
                .subscribeOn(uiScheduler)
                .subscribe({ authWithSuccess(it as Session) },
                        {})

        composite.add(disposable)
    }

    fun createUser() {

        val disposable = viewModel.createUser(username_input.text.toString(), password_input.text.toString())
                .compose(coordinator)
                .subscribeOn(uiScheduler)
                .subscribe(
                        { createdWithSuccess(it as StatusMessage) },
                        {}
                )

        composite.add(disposable)
    }

    fun createdWithSuccess(statusMessage: StatusMessage) {
        showCallToAction(statusMessage.message)
        login()
    }

    fun authWithSuccess(session: Session) {
        println(session.token)
        val intent = MainActivity.newIntent(this, session.token)
        startActivity(intent)
    }

    override fun showLoading(): Action {
        return Action { loginBtn.text = "carregando" }
    }

    override fun showErrorState(): Action {
        return Action { Log.d("teste", "teste") }
    }

    override fun reportNetworkingError(): Action {
        return Action { showCallToAction(R.string.no_connection_message_error) }
    }

    override fun hideLoading(): Action {
        return Action { loginBtn.text = "feito" }
    }

    override fun hideErrorState(): Action {
        return Action { println("tirando  error state") }
    }

    override fun showAuthError(): Action {
        return Action { showCallToAction(R.string.user_not_found_message_error) }
    }

    override fun showCreateUserError(): Action {
        return Action { showCallToAction(R.string.user_taken_message_error) } //To change body of created functions use File | Settings | File Templates.
    }

    private fun showCallToAction(callToActionText: Int) {
        Snackbar.make(root, callToActionText, Snackbar.LENGTH_LONG)
                .show()
    }

    private fun showCallToAction(actionText: String) {
        Snackbar.make(root, actionText, Snackbar.LENGTH_LONG)
                .show()
    }

    private fun releaseSubscriptions() {
        composite.clear()
    }

}
