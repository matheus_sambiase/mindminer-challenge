package br.com.takehomeproject.factories

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import br.com.presentation.user.UserViewModel

object UserFactory {

    operator fun invoke(): ViewModelProvider.Factory {
        return object : ViewModelProvider.Factory {

            override fun <T : ViewModel?> create(modelClass: Class<T>?): T {
                val fetchUserInfrastructure = UserInfrastructureFactory.fetchUser()
                val updateUserInfoInfrastructure = UserInfrastructureFactory.updateUserInfo()
                return UserViewModel(
                        fetchUser = fetchUserInfrastructure,
                        updateUserInfo = updateUserInfoInfrastructure) as T
            }

        }
    }
}