package br.com.takehomeproject.factories

import br.com.presentation.behaviors.BehaviorCoordinator
import io.reactivex.android.schedulers.AndroidSchedulers


object BehaviorsFactory {

    val uiScheduler by lazy { AndroidSchedulers.mainThread() }

    operator fun invoke(passiveView: Any): BehaviorCoordinator {
        return BehaviorCoordinator(passiveView, uiScheduler)
    }
}