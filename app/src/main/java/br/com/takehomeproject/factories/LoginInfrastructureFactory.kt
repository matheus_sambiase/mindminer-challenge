package br.com.takehomeproject.factories

import br.com.infrastructure.data.LoginWebService
import br.com.infrastructure.data.rest.WebServiceFactory
import br.com.infrastructure.domain.AuthUser
import br.com.infrastructure.domain.CreateUser
import io.reactivex.schedulers.Schedulers


object LoginInfrastructureFactory {

    private val ioScheduler = Schedulers.io()
    private val webservice = WebServiceFactory.create()

    fun authUser(): AuthUser {
        return LoginWebService(webservice, ioScheduler)
    }

    fun createUser(): CreateUser {
        return LoginWebService(webservice, ioScheduler)
    }

}