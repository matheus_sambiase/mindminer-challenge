package br.com.takehomeproject.factories

import br.com.infrastructure.data.UserWebService
import br.com.infrastructure.data.rest.WebServiceFactory
import br.com.infrastructure.domain.FetchUser
import br.com.infrastructure.domain.UpdateUserInfo
import io.reactivex.schedulers.Schedulers

object UserInfrastructureFactory {

    private val ioScheduler = Schedulers.io()
    private val webservice = WebServiceFactory.create()

    fun fetchUser(): FetchUser {
        return UserWebService(webservice, ioScheduler)
    }

    fun updateUserInfo(): UpdateUserInfo {
        return UserWebService(webservice, ioScheduler)
    }
}