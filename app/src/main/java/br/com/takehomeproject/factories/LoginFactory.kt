package br.com.takehomeproject.factories

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import br.com.presentation.login.LoginViewModel


object LoginFactory {

    operator fun invoke(): ViewModelProvider.Factory {
        return object : ViewModelProvider.Factory {

            override fun <T : ViewModel?> create(modelClass: Class<T>?): T {
                val authInfrastructure = LoginInfrastructureFactory.authUser()
                val createUserInfrastructure = LoginInfrastructureFactory.createUser()
                return LoginViewModel(
                        authUser = authInfrastructure,
                        createUser = createUserInfrastructure) as T
            }

        }
    }
}