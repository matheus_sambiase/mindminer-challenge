package br.com.presentation.errorstate

import br.com.infrastructure.domain.RequestError
import br.com.presentation.behaviors.BaseCoordinator
import br.com.presentation.behaviors.errorstate.ErrorAuthView
import br.com.presentation.behaviors.errorstate.ErrorCreateUserView
import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.ObservableTransformer
import io.reactivex.Scheduler


class ErrorStateCoordinator(private val view: Any,
                            uiScheduler: Scheduler) : BaseCoordinator(uiScheduler), ObservableTransformer<Any, Any> {

    override fun apply(upstream: Observable<Any>): ObservableSource<Any> {
        if (view is ErrorStateView) {
            return upstream
                    .subscribeOn(uiScheduler)
                    .doOnSubscribe { subscribeAndFireAction(view.hideErrorState()) }
                    .doOnError(this@ErrorStateCoordinator::handleError)
        }
        return upstream
    }

    private fun handleError(error: Throwable) {
        when (error){
            is RequestError.UserNotFound -> subscribeAndFireAction((view as ErrorAuthView).showAuthError())
            is RequestError.UserTaken -> subscribeAndFireAction((view as ErrorCreateUserView).showCreateUserError())
        }
//        if (error is RequestError)
//            subscribeAndFireAction((view as ErrorStateView).showErrorState())
    }


}