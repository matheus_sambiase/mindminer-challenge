package br.com.presentation.behaviors

import io.reactivex.Completable
import io.reactivex.Scheduler
import io.reactivex.functions.Action


abstract class BaseCoordinator(val uiScheduler: Scheduler) {

    fun subscribeAndFireAction(toPerform: Action) {
        Completable.fromAction(toPerform)
                .subscribeOn(uiScheduler)
                .subscribe()
    }
}