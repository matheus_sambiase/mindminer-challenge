package br.com.presentation.loading

import io.reactivex.functions.Action


interface LoadingView {

    fun showLoading(): Action

    fun hideLoading(): Action

}