package br.com.presentation.networkstate

import br.com.infrastructure.domain.NetworkError
import br.com.presentation.behaviors.BaseCoordinator
import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.ObservableTransformer
import io.reactivex.Scheduler


class NetworkErrorCoordinator(private val view: Any,
                              uiScheduler: Scheduler) : BaseCoordinator(uiScheduler), ObservableTransformer<Any, Any> {

    override fun apply(upstream: Observable<Any>): ObservableSource<Any> {
        return upstream
                .subscribeOn(uiScheduler)
                .doOnError(this@NetworkErrorCoordinator::handleNetworkError)
    }

    private fun handleNetworkError(throwable: Throwable) {


        if (view is NetworkErrorStateView && throwable is NetworkError) {
            subscribeAndFireAction(view.reportNetworkingError())
        }
    }


}