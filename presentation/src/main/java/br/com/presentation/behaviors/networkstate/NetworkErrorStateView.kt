package br.com.presentation.networkstate

import io.reactivex.functions.Action


interface NetworkErrorStateView {

    fun reportNetworkingError(): Action

}