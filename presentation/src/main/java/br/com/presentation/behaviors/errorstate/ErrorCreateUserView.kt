package br.com.presentation.behaviors.errorstate

import io.reactivex.functions.Action

interface ErrorCreateUserView {

    fun showCreateUserError(): Action
}