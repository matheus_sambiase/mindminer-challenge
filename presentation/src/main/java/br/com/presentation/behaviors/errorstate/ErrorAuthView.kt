package br.com.presentation.behaviors.errorstate

import io.reactivex.functions.Action

interface ErrorAuthView {

    fun showAuthError(): Action
}