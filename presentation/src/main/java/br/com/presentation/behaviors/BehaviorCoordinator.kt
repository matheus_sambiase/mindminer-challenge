package br.com.presentation.behaviors

import br.com.presentation.errorstate.ErrorStateCoordinator
import br.com.presentation.loading.LoadingCoordinator
import br.com.presentation.networkstate.NetworkErrorCoordinator
import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.ObservableTransformer
import io.reactivex.Scheduler


class BehaviorCoordinator internal constructor(
        val networkErrorCoordinator: NetworkErrorCoordinator,
        val errorStateCoordinator: ErrorStateCoordinator,
        val loadingCoordinator: LoadingCoordinator
) : ObservableTransformer<Any, Any> {

    override fun apply(upstream: Observable<Any>): ObservableSource<Any> {
        return upstream
                .compose(networkErrorCoordinator)
                .compose(errorStateCoordinator)
                .compose(loadingCoordinator)

    }

    companion object Factory {
        operator fun invoke(view: Any, uiScheduler: Scheduler): BehaviorCoordinator {
            val networkErrorCoodinator = NetworkErrorCoordinator(view, uiScheduler)
            val errorStateCoordinator = ErrorStateCoordinator(view, uiScheduler)
            val loadingCoordinator = LoadingCoordinator(view, uiScheduler)

            return BehaviorCoordinator(
                    networkErrorCoordinator = networkErrorCoodinator,
                    errorStateCoordinator = errorStateCoordinator,
                    loadingCoordinator = loadingCoordinator)
        }
    }


}