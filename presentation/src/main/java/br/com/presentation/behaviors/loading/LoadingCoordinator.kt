package br.com.presentation.loading

import br.com.presentation.behaviors.BaseCoordinator
import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.ObservableTransformer
import io.reactivex.Scheduler


class LoadingCoordinator(private val view: Any,
                         uiScheduler: Scheduler) : BaseCoordinator(uiScheduler), ObservableTransformer<Any, Any> {

    override fun apply(upstream: Observable<Any>): ObservableSource<Any> {
        if (view is LoadingView) {
            return upstream.apply {
                subscribeOn(uiScheduler)
                doOnSubscribe { subscribeAndFireAction(view.showLoading()) }
                doOnTerminate { subscribeAndFireAction(view.hideLoading()) }
            }
        }
        return upstream
    }
}