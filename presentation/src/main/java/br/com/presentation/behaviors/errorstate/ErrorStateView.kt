package br.com.presentation.errorstate

import io.reactivex.functions.Action


interface ErrorStateView {

    fun showErrorState(): Action

    fun hideErrorState(): Action

}