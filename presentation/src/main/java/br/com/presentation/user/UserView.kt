package br.com.presentation.user

import br.com.presentation.errorstate.ErrorStateView
import br.com.presentation.loading.LoadingView
import br.com.presentation.networkstate.NetworkErrorStateView


interface UserView :
        LoadingView,
        ErrorStateView,
        NetworkErrorStateView

