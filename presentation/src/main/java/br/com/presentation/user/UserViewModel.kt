package br.com.presentation.user

import android.arch.lifecycle.ViewModel
import br.com.infrastructure.data.rest.FetchUserBody
import br.com.infrastructure.data.rest.UpdateUserInfoBody
import br.com.infrastructure.domain.FetchUser
import br.com.infrastructure.domain.StatusMessage
import br.com.infrastructure.domain.UpdateUserInfo
import br.com.infrastructure.domain.User
import io.reactivex.Observable


class UserViewModel(val fetchUser: FetchUser,
                    val updateUserInfo: UpdateUserInfo) : ViewModel() {

    fun fetchUser(token: String): Observable<User> {
        return fetchUser.fetch(FetchUserBody(token))
    }

    fun updateUserInfo(token: String, password: String, user: User): Observable<StatusMessage> {
        return updateUserInfo.update(UpdateUserInfoBody(
                token = token,
                password = password,
                firstname = user.firstName,
                lastname = user.lastName
        ))
    }
}