package br.com.presentation.login

import android.arch.lifecycle.ViewModel
import br.com.infrastructure.data.rest.AuthBody
import br.com.infrastructure.data.rest.CreateUserBody
import br.com.infrastructure.domain.AuthUser
import br.com.infrastructure.domain.CreateUser
import br.com.infrastructure.domain.Session
import br.com.infrastructure.domain.StatusMessage
import io.reactivex.Observable


class LoginViewModel(val authUser: AuthUser,
                     val createUser: CreateUser) : ViewModel() {

    fun createUser(username: String, password: String): Observable<StatusMessage> {
        return createUser.create(CreateUserBody(username, password))
    }

    fun authUser(username: String, password: String): Observable<Session> {
        return authUser.auth(AuthBody(username, password))
    }
}