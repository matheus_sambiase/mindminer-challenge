package br.com.presentation.login

import br.com.presentation.behaviors.errorstate.ErrorAuthView
import br.com.presentation.behaviors.errorstate.ErrorCreateUserView
import br.com.presentation.errorstate.ErrorStateView
import br.com.presentation.loading.LoadingView
import br.com.presentation.networkstate.NetworkErrorStateView


interface LoginView :
        LoadingView,
        ErrorStateView,
        NetworkErrorStateView,
        ErrorAuthView,
        ErrorCreateUserView

